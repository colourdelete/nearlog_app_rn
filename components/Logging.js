import React from 'react';
import {View, Text} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import styles from './Styles';

const Logging = props => {
  return (
    <View style={styles.container}>
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          alignItems: 'center',
          paddingTop: 20,
        }}>
        <Icon
          name="file-document-box-multiple-outline"
          size={40}
          color="darkblue"
        />
        <Text style={styles.text}> Logging</Text>
      </View>
      <Text>
        {'\n'}
        {'\n'}
      </Text>
    </View>
  );
};

export default Logging;
