import React from 'react';
import {View, Text} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import styles from './Styles';

const Home = props => {
  return (
    <View style={styles.container}>
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          alignItems: 'center',
          paddingTop: 20,
        }}>
        <Icon name="home-outline" size={40} color="darkblue" />
        <Text style={(styles.text, styles.big)}> Home</Text>
      </View>
      <Text>
        {'\n'}
        {'\n'}
      </Text>
      <Text style={styles.text}>Your UUID:</Text>
    </View>
  );
};

export default Home;
