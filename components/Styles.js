// import React from 'react';
import StyleSheet from 'react-native';

const Styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    // height: 200
  },
  big: {
    fontSize: 40,
  },
  text: {
    color: 'darkblue',
    textAlign: 'center',
  },
});

export default Styles;
