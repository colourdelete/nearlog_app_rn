import React from 'react';
import {View, Button, StyleSheet} from 'react-native';
import Home from './Home';
import Logging from './Logging';
import Settings from './Settings';
import {createBottomTabNavigator, createAppContainer} from 'react-navigation';

const NavBar = props => {
  return (
    <View style={styles.container}>
      <Button title="Toggle Logging" accessibilityLabel="Toggle Logging" />
    </View>
  );
};

const bottomTabNavigator = createBottomTabNavigator(
  {
    Home: Home,
    Logging: Logging,
    Settings: Settings,
  },
  {
    initialRouteName: 'Home',
  },
);

function ToggleLogging(e) {
  console.log('Toggle logging.');
}

const AppContainer = createAppContainer(bottomTabNavigator);

const styles = StyleSheet.create({
  container: {
    height: 60,
    padding: 15,
    backgroundColor: 'darkblue',
  },
  Button: {
    color: 'darkblue',
    backgroundColor: 'white',
    borderRadius: 100,
  },
  text: {
    color: 'white',
    fontSize: 20,
    textAlign: 'center',
  },
});

export default NavBar;
