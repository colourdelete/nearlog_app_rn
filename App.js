import React from 'react';
import {View, ScrollView, Text, Switch} from 'react-native';
import {Header, SectionHeader, styles, SwitchRow} from './components/Elements';
import {uuid} from 'uuid';
import bleManager from 'react-native-ble-plx';

const App = () => {
  return (
    <View style={{flex: 1}}>
      {/*<Header />*/}
      {/*<ScrollView style={{flex: 1}}>*/}
      {/*  <Home />*/}
      {/*  <Logging />*/}
      {/*  <Settings />*/}
      {/*</ScrollView>*/}
      <Header title={'Nearlog'} />
      <ScrollView style={{flex: 1}}>
        <View>
          <SectionHeader title={'Home'} icon={'home'} />
        </View>
        <View>
          <SectionHeader title={'Settings'} icon={'settings-outline'} />
          <SwitchRow title={'Bluetooth Logging'} icon={'file-document-box-multiple-outline'} />
          <SwitchRow title={'Notify if possibly contacted'} />
          <SwitchRow title={'Access Server'} />
          <SwitchRow title={'Setting 4'} />
          <SwitchRow title={'Setting 5'} />
          <SwitchRow title={'Setting 6'} />
          <SwitchRow title={'Setting 7'} />
          <SwitchRow title={'Setting 8'} />
          <SwitchRow title={'Setting 9'} />
          <SwitchRow title={'Setting 10'} />
        </View>
      </ScrollView>
    </View>
  );
};

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//   },
//   text: {
//     color: 'darkslateblue',
//     fontSize: 30,
//   },
// });

export default App;
